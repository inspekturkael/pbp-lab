from django.db import models

# Create your models here.
class Note(models.Model):
    untuk = models.CharField(max_length=30)
    dari = models.CharField(max_length=30)
    title = models.CharField(max_length=50)
    message = models.TextField()