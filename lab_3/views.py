from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FriendForm
from django.contrib import messages
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login')
def index(request):
    friends = Friend.objects.all()
    return render(request, 'indexlab3.html', {'friends': friends})

@login_required(login_url='/admin/login')
def form(request):
    form = FriendForm()

    if(request.method == "POST"):
        form =  FriendForm(request.POST)
        if(form.is_valid()):
            form.save()
            messages.success(request, "Friend created successfully")
            return HttpResponseRedirect("/lab-3")

    return render(request, 'formlab3.html', {'form':form})