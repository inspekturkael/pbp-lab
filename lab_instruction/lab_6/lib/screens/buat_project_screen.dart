import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/categories_screen.dart';

import '../widgets/main_drawer.dart';
import 'category_meals_screen.dart';

class BuatProjectScreen extends StatefulWidget {
  static const routeName = '/buat_project';

  @override
  _BuatProjectScreenState createState() => _BuatProjectScreenState();

  void saveFilters(Map<String, bool> selectedFilters) {}
}

class _BuatProjectScreenState extends State<BuatProjectScreen> {
  final _formKey = GlobalKey<FormState>();
  String namaProyek = "nama_proyek";
  String kategoriProyek = "kategori_proyek";
  String bayaran = "bayaran";
  String deskripsiProyek = "deskripsi_proyek";
  String estimasiWaktu = "estimasi_waktu";
  String daftarSkillyangDibutuhkan = "daftarSkillyangDibutuhkan";
  String kontakPemilik = "kontakPemilik";
  String jumlahOrang = "jumlahOrang";
  final controllerNPro = TextEditingController(text: "Your initial value");
  final controllerKpro = TextEditingController(text: "Your initial value");
  final controllerbay = TextEditingController(text: "Your initial value");
  final controllerdeskr = TextEditingController(text: "Your initial value");
  final controllereswak = TextEditingController(text: "Your initial value");
  final controllerdafskiyadi =
      TextEditingController(text: "Your initial value");
  final controllerkonpem = TextEditingController(text: "Your initial value");
  final controllerjumor = TextEditingController(text: "Your initial value");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buat Project'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {},
          )
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(25),
            child: Text(
              'Silakan buat proyeknya Kak :D',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Nama proyek',
                    hintText: 'KitaLulus',
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Padding(padding: const EdgeInsets.symmetric(vertical: 8.0)),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Kategori proyek',
                    hintText: 'Web Development',
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Padding(padding: const EdgeInsets.symmetric(vertical: 8.0)),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Bayaran',
                    hintText: '100.000.000 (tanpa titik)',
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Padding(padding: const EdgeInsets.symmetric(vertical: 8.0)),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Deskripsi Proyek',
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Padding(padding: const EdgeInsets.symmetric(vertical: 8.0)),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Estimasi Waktu',
                    hintText: '10 Tahun',
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Padding(padding: const EdgeInsets.symmetric(vertical: 8.0)),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Daftar Skills yang Dibutuhkan',
                    hintText: 'Python, Java, Css',
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Padding(padding: const EdgeInsets.symmetric(vertical: 8.0)),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Jumlah Orang',
                    hintText: '100',
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 16.0, horizontal: 32.0),
                  child: ElevatedButton(
                    onPressed: () {
                      // Validate returns true if the form is valid, or false otherwise.
                      print(controllerNPro.text + "\n" + controllerNPro.text + "\n" + controllerbay.text);
                      // _navigateToNextScreen(context);

                      if (_formKey.currentState.validate()) {
                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Processing Data')),
                        );
                      }
                    },
                    child: const Text('Submit'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _navigateToNextScreen(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => CategoriesScreen()));
  }
}
