from django.urls import path
from . import views

app_name = 'lab_1'

urlpatterns = [
    path('', views.index, name='indexlab1'),
    path('friendlist/', views.friend_list, name='friend_list')
]
