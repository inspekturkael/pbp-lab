from django.urls import path
from . import views

app_name = 'lab_4'

urlpatterns = [
    path('', views.index, name='indexlab4'),
    path('add_note/', views.createform, name='createform4'),
    path('card_note/<int:note_id>/', views.note_card, name='cardnote'),
    path('edit_note/<int:note_id>/', views.editform, name='editform4'),
    path('delete_note/<int:note_id>/', views.deleteform, name='deleteform4')
]
