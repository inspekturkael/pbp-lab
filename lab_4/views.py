from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'indexlab4.html', response)

def createform(request):
    form = NoteForm()

    if(request.method == "POST"):
        form =  NoteForm(request.POST)
        if(form.is_valid()):
            form.save()
            messages.success(request, "Note created successfully")
            return HttpResponseRedirect("/lab-4")
    return render(request, 'formlab4.html', {'form':form})

#belum
def editform(request, note_id):
    note = Note.objects.get(id=note_id)
    return render(request, 'editform4.html', {'note':note})

def deleteform(request, note_id):
    Note.objects.get(id=note_id).delete()
    messages.error(request, "Note deleted successfully")
    return HttpResponseRedirect("/lab-4")

def note_card(request, note_id):
    note = Note.objects.get(id=note_id)
    return render(request, 'notecard.html', {'note':note})